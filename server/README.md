

# Server side application - REST API

<details>
<summary>Post</summary>

## Get posts

**Request**

page of given size

```
> GET /posts?page=0&size=10
```

**Success Response**

```
< 200 OK

{
    "dtoList": [
        ...
        {
            "id": 1,
            "title": "postTitleExample",
            "content": "postContentExample",
            "categoryId": 1,
            "created": "2020-11-27"
        }
        ...
    ],
    "totalPages": 1
}
```

---

## Get posts by category

**Request**

page from category of given size

```
> GET /posts?page=0&size=10&category=1
```

**Success Response**

```
< 200 OK

{
    "dtoList": [
        ...
        {
            "id": 1,
            "title": "postTitleExample",
            "content": "postContentExample",
            "categoryId": 1,
            "created": "2020-11-27"
        }
        ...
    ],
    "totalPages": 1
}
```

---

## Get post by id

**Request**

```
> GET /posts/{id}
```

**Success Response**

```
< 200 OK

{
    "id": {id},
    "title": "postTitleExample",
    "content": "postContentExample",
    "categoryId": 1,
    "created": "2020-11-27"
}
```

**Error Response**

If post with `id` does not exist

```
< 404 Not Found
```

---

## Create new post 

**Request**

```
> POST /posts

Authorization: Basic <token>
Content-Type: application/json

{
    "title" : "title",
    "content" : "content",
    "categoryId" : 1
}

```

**Success Response**

```
< 201 Created
```

**Bad Request Error Response**

If category with `id` does not exist

```
< 400 Bad Request
```

**Unthorized Error Response**
```
< 401 Unauthorized
```
---

## Update post 

```
> PUT /posts/{id}

Authorization: Basic <token>
Content-Type: application/json

{
    "title" : "title",
    "content" : "content",
    "categoryId" : 1
}
```

**Success Response**
```
< 204 No Content
```

**Not Found Error Response**

If post with `id` not found
```
< 404 Not Found
```

**Bad Request Error Response**

If post DTO is invalid
```
< 400 Bad Request
```

**Unthorized Error Response**
```
< 401 Unauthorized
```

---
## Add tags to post

```
> PUT /posts/{id}/addTags

Authorization: Basic <token>
Content-Type: application/json

{
	"ids" : [...,13,...]
}
```

**Success Response**
```
< 204 No Content
```

**Not Found Error Response**

If `post` with `id` not found
```
< 404 Not Found
```

**Bad Request Error Response**

If every `tag` `id` is invalid
```
< 400 Bad Request
```

**Unthorized Error Response**
```
< 401 Unauthorized
```
---

## Remove tags from post

```
> PUT /posts/{id}/removeTags

Authorization: Basic <token>
Content-Type: application/json

{
	"ids" : [...,13,...]
}
```

**Success Response**
```
< 204 No Content
```

**Not Found Error Response**

If `post` with `id` not found
```
< 404 Not Found
```

**Bad Request Error Response**

If every `tag` `id` is invalid
```
< 400 Bad Request
```

**Unthorized Error Response**
```
< 401 Unauthorized
```

---

## Delete post 

```
> DELETE /posts/{id}

Authorization: Basic <token>
```

**Success Response**
```
< 204 No Content
```

**Not Found Error Response**

If post with `id` not found
```
< 404 Not Found
```

**Unthorized Error Response**
```
< 401 Unauthorized
```

</details>

---

<details>
<summary>Category</summary>

## Get all categories

**Request**

```
> GET /categories
```

**Success Response**

```
< 200 OK

[
    ...
    {
        "name": "categoryName",
        "description": "categoryDescription",
        "id": 1
    }
    ...
]
```

---

## Get Category by id

**Request**

```
> GET /categories/{id}
```

**Success Response**

Retruns all category's details except `posts`

```
< 200 OK

{
    "name": "categoryName",
    "description": "categoryDescription",
    "id": 1
}
```

**Not Found Error Response**

If category with `id` does not exist

```
< 404 Not Found
```

---

## Create new category 

**Request**

```
> POST /categories

Authorization: Basic <token>
Content-Type: application/json

{
    "name": "categoryName",
    "description": "categoryDescription"
}

```

**Success Response**

```
< 201 Created
```

**Bad Request Error Response**

If category with `id` does not exist

```
< 400 Bad Request
```

**Unthorized Error Response**
```
< 401 Unauthorized
```

---

## Update category 

```
> PUT /categories/{id}

Authorization: Basic <token>
Content-Type: application/json

{
    "name": "categoryName",
    "description": "categoryDescription"
}
```

**Success Response**
```
< 204 No Content
```

**Not Found Error Response**

If category with `id` not found
```
< 404 Not Found
```

**Bad Request Error Response**

If category DTO is invalid
```
< 400 Bad Request
```

**Unthorized Error Response**
```
< 401 Unauthorized
```

---

## Delete category 
```
> DELETE /categories/{id}

Authorization: Basic <token>
```

**Success Response**
```
< 204 No Content
```

**Not Found Error Response**

If category with `id` not found
```
< 404 Not Found
```

**Unthorized Error Response**
```
< 401 Unauthorized
```
</details>

---

<details>
<summary>Tag</summary>

## Get all tags

**Request**

```
> GET /tags
```

**Success Response**

```
< 200 OK

[
    ...
    {
        "name": "tagName",
        "id": 1
    }
    ...
]
```

---

## Get tag by id

**Request**

```
> GET /tags/{id}
```

**Success Response**

```
< 200 OK

{
    "name": "tagName",
    "id": {id}
}
```

**Error Response**

If tag with `id` does not exist

```
< 404 Not Found
```

---

## Create new tag 

**Request**

```
> POST /tags

Authorization: Basic <token>
Content-Type: application/json

{
    "name": "tagName"
}

```

**Success Response**

```
< 201 Created
```

**Bad Request Error Response**

If tag with `id` does not exist

```
< 400 Bad Request
```

**Unthorized Error Response**
```
< 401 Unauthorized
```

---

## Update tag 

```
> PUT /tags/{id}

Authorization: Basic <token>
Content-Type: application/json

{
    "name": "updatedTagName"
}
```

**Success Response**
```
< 204 No Content
```

**Not Found Error Response**

If tag with `id` not found
```
< 404 Not Found
```

**Bad Request Error Response**

If tag DTO is invalid
```
< 400 Bad Request
```


**Unthorized Error Response**
```
< 401 Unauthorized
```

---

## Delete tag 
```
> DELETE /tags/{id}

Authorization: Basic <token>
```
**Success Response**
```
< 204 No Content
```

**Not Found Error Response**

If tag with `id` not found
```
< 404 Not Found
```

**Unthorized Error Response**
```
< 401 Unauthorized
```

</details>

---

<details>
<summary>Comment</summary>

## Get all comments

**Request**

```
> GET /comments
```

**Success Response**

```
< 200 OK

[
    ...
    {
		"content"  :  "commentContent",
		"toPostId"  :  1,
		"id" : 2
	}
    ...
]
```

---

## Get all comments from post

**Request**

```
> GET /comments?post=1&page=0&size=10
```

**Success Response**

```
< 200 OK

[
    ...
    {
		"content"  :  "commentContent",
		"toPostId"  :  1,
		"id" : 2
	}
    ...
]
```

**Error Response**

If post with `id` does not exist

```
< 404 Not Found
```

---


## Get comment by id

**Request**

```
> GET /comments/{id}
```

**Success Response**
```
< 200 OK

{
	"content"  :  "updatedCommentContent",
	"toPostId"  :  1
	"id" : {id}
}
```

**Not Found Error Response**

If comment with `id` does not exist

```
< 404 Not Found
```

---

## Create new comment 

**Request**

```
> POST /comments

Content-Type: application/json

{
	"content"  :  "commentContent",
	"toPostId"  :  1
}
```

**Success Response**

```
< 201 Created
```

**Error Response**

If post with `toPostId` does not exist

```
< 400 Bad Request
```

---

## Update comment 

```
> PUT /comments/{id}

Authorization: Basic <token>
Content-Type: application/json

{
	"content"  :  "updatedCommentContent",
	"toPostId"  :  1
}
```

**Success Response**
```
< 204 No Content
```

**Not Found Error Response**

If comment with `id` not found
```
< 404 Not Found
```

**Bad Request Error Response**

If comment DTO is invalid
```
< 400 Bad Request
```

**Unthorized Error Response**
```
< 401 Unauthorized
```

---

## Delete comment 
```
> DELETE /comments/{id}

Authorization: Basic <token>
```
**Success Response**
```
< 204 No Content
```

**Not Found Error Response**

If comment with `id` not found
```
< 404 Not Found
```

**Unthorized Error Response**
```
< 401 Unauthorized
```

</details>

---