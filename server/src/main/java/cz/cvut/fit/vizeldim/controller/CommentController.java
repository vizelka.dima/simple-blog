package cz.cvut.fit.vizeldim.controller;

import cz.cvut.fit.vizeldim.dto.create.CommentCreateDTO;
import cz.cvut.fit.vizeldim.dto.response.CommentDTO;
import cz.cvut.fit.vizeldim.dto.response.PageDTO;
import cz.cvut.fit.vizeldim.entity.Comment;
import cz.cvut.fit.vizeldim.service.CommentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

@RestController
@RequestMapping("/comments")
public class CommentController extends MyController<Comment, CommentDTO, CommentCreateDTO>{
    private final CommentService commentService;

    @Autowired
    public CommentController(CommentService commentService) { this.commentService = commentService; }

    @Override
    protected CommentService getService() {
        return commentService;
    }

    @GetMapping(params = {"page", "size", "post"})
    @ResponseStatus(HttpStatus.OK)
    public PageDTO<CommentDTO> readPageInPost(@RequestParam int page,
                                              @RequestParam int size,
                                              @RequestParam long post )
    {
        return new PageDTO<>(
            getService()
                .findPageFromPost(PageRequest.of(page,size), post)
                .orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND))
        );
    }
}
