package cz.cvut.fit.vizeldim.entity;

import org.hibernate.annotations.Type;

import javax.persistence.*;
import javax.persistence.Entity;
import javax.validation.constraints.NotNull;
import java.time.LocalDate;
import java.util.*;

@Entity
public class Post {
    @Id
    @GeneratedValue
    private Long id;

    @NotNull
    private String title;

    @NotNull
    @Type(type = "org.hibernate.type.TextType")
    private String content;

    @NotNull
    private LocalDate created;

    @NotNull
    @ManyToOne
    private Category category;

    @NotNull
    @ManyToMany
    private Set<Tag> tags;

    @OneToMany(mappedBy = "toPost", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    private List<Comment> comments;

    public Post(String title, String content, Category category) {
        this.title = title;
        this.content = content;
        this.created = LocalDate.now();
        this.category = category;
        this.tags = new HashSet<>();
    }

    public Post() { }

    public Long getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public LocalDate getCreated() {
        return created;
    }

    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }

    public Set<Tag> getTags() {
        return tags;
    }

    public void setTags(Set<Tag> tags) {
        this.tags = tags;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Post post = (Post) o;
        return Objects.equals(id, post.id) &&
                Objects.equals(title, post.title) &&
                Objects.equals(content, post.content) &&
                Objects.equals(created, post.created) &&
                Objects.equals(category, post.category) &&
                Objects.equals(tags, post.tags);
    }
}
