package cz.cvut.fit.vizeldim.controller;

import cz.cvut.fit.vizeldim.dto.create.CategoryCreateDTO;
import cz.cvut.fit.vizeldim.dto.response.CategoryDTO;
import cz.cvut.fit.vizeldim.entity.Category;
import cz.cvut.fit.vizeldim.service.CategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/categories")
public class CategoryController extends MyController<Category, CategoryDTO, CategoryCreateDTO>{
    private final CategoryService categoryService;

    @Autowired
    public CategoryController(CategoryService categoryService) { this.categoryService = categoryService; }

    @Override
    protected CategoryService getService() { return categoryService; }

    @GetMapping
    @ResponseStatus(HttpStatus.OK)
    public List<CategoryDTO> readAll()
    {
        return getService().findAll();
    }
}