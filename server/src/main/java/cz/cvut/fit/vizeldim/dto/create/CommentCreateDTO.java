package cz.cvut.fit.vizeldim.dto.create;

import java.util.Objects;

public class CommentCreateDTO{
    protected final String content;

    protected final Long toPostId;

    public CommentCreateDTO(String content,Long toPostId) {
        this.content = content;
        this.toPostId = toPostId;
    }

    public String getContent() {
        return content;
    }

    public Long getToPostId() {
        return toPostId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CommentCreateDTO that = (CommentCreateDTO) o;
        return Objects.equals(content, that.content) &&
                Objects.equals(toPostId, that.toPostId);
    }

}
