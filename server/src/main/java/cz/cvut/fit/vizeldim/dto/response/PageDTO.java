package cz.cvut.fit.vizeldim.dto.response;

import org.springframework.data.domain.Page;

import java.util.List;

public class PageDTO<DTO> {
    public List<DTO> dtoList;
    public int totalPages;

    public PageDTO(Page<DTO> page) {
        dtoList = page.getContent();
        totalPages = page.getTotalPages();
    }
}