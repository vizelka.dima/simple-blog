package cz.cvut.fit.vizeldim.repository;

import cz.cvut.fit.vizeldim.entity.Category;
import cz.cvut.fit.vizeldim.entity.Post;
import cz.cvut.fit.vizeldim.entity.Tag;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PostRepository extends JpaRepository<Post,Long> {
    Page<Post> findAllByCategory(Pageable page, Category category);
    Page<Post> findAllByTagsContains(Pageable page, Tag tag);
}