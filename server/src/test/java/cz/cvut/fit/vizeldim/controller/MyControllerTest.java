package cz.cvut.fit.vizeldim.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import cz.cvut.fit.vizeldim.dto.response.PageDTO;
import cz.cvut.fit.vizeldim.service.MyService;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
public abstract class MyControllerTest<Entity, DTO, CreateDTO>{
    @Autowired
    protected MockMvc mockMvc;

    @Autowired
    protected ObjectMapper objectMapper;

    protected static final long id = 13;

    protected abstract MyService<Entity,DTO,CreateDTO> getService();
    protected abstract DTO getDTO_Test();
    protected abstract CreateDTO getCreateDTO_Test();

    protected abstract String getUrl();

    @Test
    public void testReadOne() throws Exception {
        Mockito
            .when(getService().findByIdAsDTO(id))
            .thenReturn(Optional.of(getDTO_Test()));

        MvcResult result = mockMvc
                .perform(get("/{url}/{id}",getUrl(),id))
                .andExpect(status().isOk())
                .andReturn();

        String response
                = result.getResponse().getContentAsString();

        String expectedResponseBody
                = objectMapper.writeValueAsString(getDTO_Test());

        assertThat(response)
                .isEqualToIgnoringWhitespace(expectedResponseBody);
    }

    @Test
    public void testReadOneNotExist() throws Exception {
        long idNotExist = 1;
        Mockito.when(getService().findByIdAsDTO(id)).thenReturn(Optional.of(getDTO_Test()));

        MvcResult result = mockMvc
                .perform(get("/{url}/{id}",getUrl(),idNotExist))
                .andExpect(status().isNotFound())
                .andReturn();

        String response = result.getResponse().getContentAsString();

        assertThat(response).isEmpty();
    }

    @Test
    public void testReadPage() throws Exception {
        List<DTO> DTOList = new ArrayList<>();
        DTOList.add(getDTO_Test());

        Page<DTO> dtoPage = new PageImpl<>(DTOList);

        PageDTO<DTO> myPage = new PageDTO<>(dtoPage);

        Mockito
                .when(getService().findPage(PageRequest.of(0,10)))
                .thenReturn(dtoPage);

        MvcResult result = mockMvc
                .perform( get("/{url}", getUrl())
                        .param("page", "0")
                        .param("size", "10") )
                .andExpect(status().isOk())
                .andReturn();

        String response = result.getResponse().getContentAsString();

        String expectedResponseBody =
                objectMapper.writeValueAsString(myPage);

        assertThat(response)
                .isEqualToIgnoringWhitespace(expectedResponseBody);
    }

    @Test
    @WithMockUser(username = "admin", password = "password", roles = "USER")
    public void testCreate() throws Exception {
        Long rndId = (long)13;
        Mockito.when(getService().create(getCreateDTO_Test())).thenReturn(Optional.of(rndId));
        mockMvc
                .perform( post("/{url}", getUrl())
                        .contentType("application/json")
                        .content(objectMapper.writeValueAsString(getCreateDTO_Test())) )
                .andExpect(status().isCreated());
    }

    @Test
    public void testCreateUnauthorized() throws Exception {
        Long rndId = (long)13;
        Mockito.when(getService().create(getCreateDTO_Test())).thenReturn(Optional.of(rndId));
        mockMvc
                .perform( post("/{url}", getUrl())
                        .contentType("application/json")
                        .content(objectMapper.writeValueAsString(getCreateDTO_Test())) )
                .andExpect(status().isUnauthorized());
    }


    @Test
    @WithMockUser(username = "admin", password = "password", roles = "USER")
    public void testCreateFail() throws Exception {
        Mockito
                .when(getService().create(getCreateDTO_Test()))
                .thenReturn(Optional.empty()); //RANDOM id

        mockMvc
                .perform( post("/{url}", getUrl())
                        .contentType("application/json")
                        .content(objectMapper.writeValueAsString(getCreateDTO_Test())) )
                .andExpect(status().isBadRequest());
    }

    @Test
    @WithMockUser(username = "admin", password = "password", roles = "USER")
    public void testUpdate() throws Exception {
        Mockito
                .when(getService().update(id,getCreateDTO_Test()))
                .thenReturn(true);

        Mockito
                .when(getService().isEmpty(getCreateDTO_Test()))
                .thenReturn(false);

        mockMvc
                .perform( put("/{url}/{id}",getUrl(),id)
                        .contentType("application/json")
                        .content(objectMapper.writeValueAsString(getCreateDTO_Test())) )
                .andExpect(status().isNoContent());
    }

    @Test
    public void testUpdateUnauthorized() throws Exception {
        Mockito
                .when(getService().update(id,getCreateDTO_Test()))
                .thenReturn(true);

        Mockito
                .when(getService().isEmpty(getCreateDTO_Test()))
                .thenReturn(false);

        mockMvc
                .perform( put("/{url}/{id}",getUrl(),id)
                        .contentType("application/json")
                        .content(objectMapper.writeValueAsString(getCreateDTO_Test())) )
                .andExpect(status().isUnauthorized());
    }

    @Test
    @WithMockUser(username = "admin", password = "password", roles = "USER")
    public void testUpdateFailBadRequest() throws Exception {
        Mockito
                .when(getService().update(id,getCreateDTO_Test()))
                .thenReturn(true);

        Mockito
                .when(getService().isEmpty(getCreateDTO_Test()))
                .thenReturn(true);

        mockMvc
                .perform( put("/{url}/{id}",getUrl(),id)
                        .contentType("application/json")
                        .content(objectMapper.writeValueAsString(getCreateDTO_Test())) )
                .andExpect(status().isBadRequest());
    }

    @Test
    @WithMockUser(username = "admin", password = "password", roles = "USER")
    public void testUpdateFailIdNotExist() throws Exception {
        Mockito
                .when(getService().update(id,getCreateDTO_Test()))
                .thenReturn(false);

        Mockito
                .when(getService().isEmpty(getCreateDTO_Test()))
                .thenReturn(false);

        mockMvc
                .perform( put("/{url}/{id}", getUrl(),id)
                        .contentType("application/json")
                        .content(objectMapper.writeValueAsString(getCreateDTO_Test())) )
                .andExpect(status().isNotFound());
    }

    @Test
    @WithMockUser(username = "admin", password = "password", roles = "USER")
    public void testDelete() throws Exception {
        mockMvc
                .perform(delete("/{url}/{id}", getUrl(),id))
                .andExpect(status().isNoContent());
    }

    @Test
    public void testDeleteUnuthorized() throws Exception {
        mockMvc
                .perform(delete("/{url}/{id}", getUrl(),id))
                .andExpect(status().isUnauthorized());
    }
}