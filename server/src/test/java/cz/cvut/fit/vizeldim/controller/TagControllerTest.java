package cz.cvut.fit.vizeldim.controller;

import cz.cvut.fit.vizeldim.dto.create.TagCreateDTO;
import cz.cvut.fit.vizeldim.dto.response.TagDTO;
import cz.cvut.fit.vizeldim.entity.Tag;
import cz.cvut.fit.vizeldim.service.TagService;
import org.springframework.boot.test.mock.mockito.MockBean;

public class TagControllerTest extends MyControllerTest<Tag, TagDTO, TagCreateDTO>{
    @MockBean
    private TagService service;

    private static final TagDTO tagDTO = new TagDTO(id, "TestName");
    private static final TagCreateDTO tagCreateDTO = new TagCreateDTO("TestName");

    @Override
    protected TagService getService() { return service; }

    @Override
    protected TagDTO getDTO_Test() { return tagDTO; }

    @Override
    protected TagCreateDTO getCreateDTO_Test() { return tagCreateDTO; }

    @Override
    protected String getUrl() { return "tags"; }
}

//TODO: TagController specific tests