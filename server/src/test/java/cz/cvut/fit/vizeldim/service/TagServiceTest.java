package cz.cvut.fit.vizeldim.service;

import cz.cvut.fit.vizeldim.dto.create.TagCreateDTO;
import cz.cvut.fit.vizeldim.dto.response.TagDTO;
import cz.cvut.fit.vizeldim.entity.Category;
import cz.cvut.fit.vizeldim.entity.Post;
import cz.cvut.fit.vizeldim.entity.Tag;
import cz.cvut.fit.vizeldim.repository.TagRepository;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;

import java.util.*;
import java.util.stream.Collectors;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.*;

@SpringBootTest
public class TagServiceTest {
    @MockBean
    private TagRepository repository;

    @InjectMocks
    @Autowired
    private TagService service;

    private static final String testName = "name";
    private static final String testNameOther = "name2";

    private static final Tag testEntity = new Tag(testName, Collections.emptyList());
    private static final TagDTO testDTO = new TagDTO(testEntity.getId(),testName);
    private static final TagCreateDTO testCreateDTO = new TagCreateDTO(testName);

    private static final Tag testEntityOther = new Tag(testNameOther,Collections.emptyList());
    private static final TagCreateDTO testCreateDTOOther = new TagCreateDTO(testNameOther);

    private static final TagCreateDTO testCreateDTOInvalid = new TagCreateDTO(null);
    private static final TagCreateDTO testCreateDTOEmpty = new TagCreateDTO(null);

    private static final Post testPost = new Post("title",
                                                "content",
                                                        new Category("name",
                                                                "description"));

    @Test
    public void testGetRep(){ assertThat(service.getRep()).isEqualTo(repository); }

    @Test
    public void testIsValid(){
        assertTrue(service.isValid(testCreateDTO));
        assertFalse(service.isValid(testCreateDTOInvalid));
    }

    @Test
    public void testIsEmpty(){
        assertFalse(service.isEmpty(testCreateDTO));
        assertTrue(service.isEmpty(testCreateDTOEmpty));
    }

    @Test
    public void testToDTO(){
        TagDTO dto = service.toDTO(testEntity);
        assertThat(dto).isEqualTo(testDTO);
    }

    @Test
    public void testToEntity(){
        assertThat(service.toEntity(testCreateDTO))
                .isEqualTo(Optional.of(testEntity));
    }

    @Test
    public void testToEntityInvalid(){
        assertThat(service.toEntity(testCreateDTOInvalid))
                .isEqualTo(Optional.empty());
    }

    @Test
    public void testUpdateEntity(){
        assertThat(service.updateEntity(testEntity, testCreateDTOOther))
                .isEqualTo(testEntityOther);

        //ROLLBACK
        testEntity.setName(testName);
        testEntity.setPosts(Collections.emptyList());
    }

    @Test
    public void testCreateInvalid(){
        assertEquals(Optional.empty(), service.create(testCreateDTOInvalid));
    }

    @Test
    public void testFindByIdAsDTO(){
        long id = 13; //Some random id for Mock

        Mockito
                .when(service.getRep().findById(id))
                .thenReturn(Optional.of(testEntity));

        assertThat(service.findByIdAsDTO(id)).
                isEqualTo(Optional.of(testDTO));
    }

    @Test
    public void testFindByIdNotExist(){
        long id = 13; //Some random id

        Mockito
                .when(service.getRep().findById(id))
                .thenReturn(Optional.empty());

        Optional<TagDTO> optionalFoundTagDTO = service.findByIdAsDTO(id);

        assertThat(optionalFoundTagDTO)
                .isEqualTo(Optional.empty());
    }

    @Test
    public void testFindByIdAsEntity(){
        long id = 13; //Some random id

        Mockito
                .when(service.getRep().findById(id))
                .thenReturn(Optional.of(testEntity));

        assertThat(service.findByIdAsEntity(id))
                .isEqualTo(Optional.of(testEntity));
    }

    @Test
    public void testFindByIdAsEntityNotExist(){
        long id = 13; //Some random id

        Mockito
                .when(service.getRep().findById(id))
                .thenReturn(Optional.empty());

        assertThat(service.findByIdAsEntity(id))
                .isEqualTo(Optional.empty());
    }

    @Test
    public void testUpdate(){
        long id = 13; //Some random id for mock

        Mockito.when(service.getRep().findById(id))
                .thenReturn(Optional.of(testEntity));

        assertTrue(service.update(id, testCreateDTO));
    }

    @Test
    public void testUpdateNotInDB(){
        long id = 13; //Some random id for mock

        Mockito
                .when(service.getRep().findById(id))
                .thenReturn(Optional.empty());

        assertFalse(service.update(id, testCreateDTO));
    }

    @Test
    public void testFindPage(){
        List<Tag> tagList = new ArrayList<>();
        tagList.add(testEntity);
        tagList.add(testEntityOther);

        Pageable page = PageRequest.of(0,10);
        Page<Tag> tagPage = new PageImpl<>(tagList);

        Mockito
                .when(service.getRep().findAll(page))
                .thenReturn(tagPage);

        assertThat(service.findPage(page))
                .isEqualTo(tagPage.map(service::toDTO));
    }

    /*-----------------------ONLY TAG----------------------------*/

    @Test
    public void testFindAllFromPost(){
        List<Tag> postList = new ArrayList<>();
        postList.add(testEntity);
        postList.add(testEntityOther);

        Mockito
                .when(repository.findAllByPostsContains(testPost))
                .thenReturn(postList);

        assertThat(service.findAllFromPost(testPost))
                .isEqualTo(postList.stream().map(service::toDTO).collect(Collectors.toList()));
    }

    @Test
    public void testGetIdNull(){
        assertEquals(Optional.empty(),service.getId(testEntity));
    }

    @Test
    public void testFindAll(){
        List<Tag> tagList = new ArrayList<>();
        tagList.add(testEntity);
        tagList.add(testEntityOther);

        Mockito
                .when(repository.findAll())
                .thenReturn(tagList);

        assertThat(service.findAll())
                .isEqualTo(
                        tagList
                                .stream()
                                .map(service::toDTO)
                                .collect(Collectors.toList()));
    }
}
