package cz.cvut.fit.vizeldim.controller;

import cz.cvut.fit.vizeldim.dto.create.IdList;
import cz.cvut.fit.vizeldim.dto.create.PostCreateDTO;
import cz.cvut.fit.vizeldim.dto.response.PageDTO;
import cz.cvut.fit.vizeldim.dto.response.PostDTO;
import cz.cvut.fit.vizeldim.entity.Post;
import cz.cvut.fit.vizeldim.entity.Tag;
import cz.cvut.fit.vizeldim.service.PostService;
import cz.cvut.fit.vizeldim.service.TagService;
import org.assertj.core.util.Lists;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MvcResult;

import java.time.LocalDate;
import java.util.*;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

public class PostControllerTest extends MyControllerTest<Post, PostDTO, PostCreateDTO>{
    @MockBean
    private PostService service;

    @MockBean
    private TagService tagService;

    @Override
    protected PostService getService() {
        return service;
    }

    private static final PostDTO postDTO = new PostDTO(id, "TestName", "TestContent",LocalDate.now(),id);
    private static final PostCreateDTO postCreateDTO = new PostCreateDTO("TestName", "TestContent", id);

    @Override
    protected PostDTO getDTO_Test() { return postDTO; }

    @Override
    protected PostCreateDTO getCreateDTO_Test() { return postCreateDTO; }

    @Override
    protected String getUrl() { return "posts"; }

    @Test
    public void testReadPageInCategory() throws Exception {
        List<PostDTO> postDTOList = new ArrayList<>();
        postDTOList.add(postDTO);

        Pageable page = PageRequest.of(0,10);
        Page<PostDTO> postPage = new PageImpl<>(postDTOList);
        PageDTO<PostDTO> postDTOPage = new PageDTO<>(postPage);

        Mockito.when(service
                        .findPageFromCategory(page,13))
                        .thenReturn(Optional.of(postPage));

        MvcResult result = mockMvc
                .perform(get("/posts")
                        .param("page", "0")
                        .param("size","10")
                        .param("category","13"))
                .andExpect(status().isOk())
                .andReturn();

        String response = result.getResponse().getContentAsString();

        String expectedResponseBody =
                objectMapper.writeValueAsString(postDTOPage);

        assertThat(response)
                .isEqualToIgnoringWhitespace(expectedResponseBody);
    }

    @Test
    public void testReadPageInCategoryNotExist() throws Exception {
        Pageable page = PageRequest.of(0,10);

        Mockito.when(service
                        .findPageFromCategory(page,13))
                        .thenReturn(Optional.empty());

        mockMvc
                .perform(get("/posts")
                        .param("page", "0")
                        .param("size","10")
                        .param("category","13"))
                .andExpect(status().isNotFound());
    }

    @Test
    public void testReadPageWithTag() throws Exception {
        List<PostDTO> postDTOList = new ArrayList<>();
        postDTOList.add(postDTO);

        Pageable page = PageRequest.of(0,10);
        Page<PostDTO> postPage = new PageImpl<>(postDTOList);
        PageDTO<PostDTO> postDTOPage = new PageDTO<>(postPage);

        Tag testTag = new Tag("tagName", Collections.emptyList());

        Mockito.when(tagService
                .findByIdAsEntity((long)13))
                .thenReturn(Optional.of(testTag));

        Mockito.when(service
                .findPageWithTag(page,testTag))
                .thenReturn(postPage);

        MvcResult result = mockMvc
                .perform(get("/posts")
                        .param("page", "0")
                        .param("size","10")
                        .param("tag","13"))
                .andExpect(status().isOk())
                .andReturn();

        String response = result.getResponse().getContentAsString();

        String expectedResponseBody =
                objectMapper.writeValueAsString(postDTOPage);

        assertThat(response)
                .isEqualToIgnoringWhitespace(expectedResponseBody);
    }

    @Test
    public void testReadPageWithTagNotExist() throws Exception {
        Mockito.when(tagService
                        .findByIdAsEntity((long)13))
                        .thenReturn(Optional.empty());

        mockMvc
                .perform(get("/posts")
                        .param("page", "0")
                        .param("size","10")
                        .param("tag","13"))
                .andExpect(status().isNotFound());
    }

    @Test
    @WithMockUser(username = "admin", password = "password", roles = "USER")
    public void testAddTagsBadRequest() throws Exception {
        Mockito.when(tagService
                .findByIdsAsEntities(any()))
                .thenReturn(Collections.emptySet());

        Optional<Post> post = service.toEntity(postCreateDTO);
        Mockito.when(service
                .findByIdAsEntity(any()))
                .thenReturn(post);

        IdList idList = new IdList();
        idList.ids = Lists.emptyList();
        mockMvc
                .perform(
                        put("/posts/1/addTags")
                        .contentType("application/json")
                        .content(objectMapper.writeValueAsString(idList))
                )
                .andExpect(status().isBadRequest());
    }

    @Test
    @WithMockUser(username = "admin", password = "password", roles = "USER")
    public void testRemoveTagsBadRequest() throws Exception {
        Mockito.when(tagService
                .findByIdsAsEntities(any()))
                .thenReturn(Collections.emptySet());

        Optional<Post> post = service.toEntity(postCreateDTO);
        Mockito.when(service
                .findByIdAsEntity(any()))
                .thenReturn(post);

        IdList idList = new IdList();
        idList.ids = Lists.emptyList();
        mockMvc
                .perform(
                        put("/posts/1/removeTags")
                                .contentType("application/json")
                                .content(objectMapper.writeValueAsString(idList))
                )
                .andExpect(status().isBadRequest());
    }
}
