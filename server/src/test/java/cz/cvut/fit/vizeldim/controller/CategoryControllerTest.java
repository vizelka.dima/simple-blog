package cz.cvut.fit.vizeldim.controller;

import cz.cvut.fit.vizeldim.dto.create.CategoryCreateDTO;
import cz.cvut.fit.vizeldim.dto.response.CategoryDTO;
import cz.cvut.fit.vizeldim.entity.Category;
import cz.cvut.fit.vizeldim.service.CategoryService;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.MvcResult;

import java.util.ArrayList;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

public class CategoryControllerTest extends MyControllerTest<Category, CategoryDTO, CategoryCreateDTO>{
    @MockBean
    private CategoryService service;

    private static final CategoryDTO categoryDTO = new CategoryDTO(id,"testName","testDescription");
    private static final CategoryCreateDTO categoryCreateDTO = new CategoryCreateDTO("testName","testDescription");

    @Override
    protected CategoryService getService() { return service; }

    @Override
    protected CategoryDTO getDTO_Test() { return categoryDTO; }

    @Override
    protected CategoryCreateDTO getCreateDTO_Test() { return categoryCreateDTO; }

    @Override
    protected String getUrl() { return "categories"; }

    @Test
    public void testReadAll() throws Exception {
        List<CategoryDTO> categoryDTOList = new ArrayList<>();
        categoryDTOList.add(categoryDTO);

        Mockito.when(service.findAll()).thenReturn(categoryDTOList);
        MvcResult result = mockMvc
                .perform(get("/categories"))
                .andExpect(status().isOk())
                .andReturn();

        String response = result.getResponse().getContentAsString();

        String expectedResponseBody =
                objectMapper.writeValueAsString(categoryDTOList);

        assertThat(response)
                .isEqualToIgnoringWhitespace(expectedResponseBody);
    }
}