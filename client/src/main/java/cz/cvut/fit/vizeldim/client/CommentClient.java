package cz.cvut.fit.vizeldim.client;

import cz.cvut.fit.vizeldim.dto.create.CommentCreateDTO;
import cz.cvut.fit.vizeldim.dto.response.page.CommentPageDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

@Service
public class CommentClient {
    private final RestTemplate restTemplate;
    private static final String URL = "/comments";
    private static final String COLLECTION_POST_PAGED_URI = "/?post={postId}&page={page}&size={size}";

    @Autowired
    public CommentClient(RestTemplate restTemplate) {
        this.restTemplate = restTemplate;
    }

    public CommentPageDTO getCommentsFromPost(Long postId, int page, int size){
        return restTemplate.getForObject(
                URL + COLLECTION_POST_PAGED_URI,
                CommentPageDTO.class,
                postId,
                page,
                size);
    }

    public void addComment(CommentCreateDTO comment) {
        restTemplate.postForObject(URL,comment, Long.class);
    }
}