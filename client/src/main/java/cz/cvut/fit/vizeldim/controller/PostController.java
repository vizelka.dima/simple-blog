package cz.cvut.fit.vizeldim.controller;

import cz.cvut.fit.vizeldim.client.CategoryClient;
import cz.cvut.fit.vizeldim.client.CommentClient;
import cz.cvut.fit.vizeldim.client.PostClient;
import cz.cvut.fit.vizeldim.client.TagClient;
import cz.cvut.fit.vizeldim.dto.create.PostCreateDTO;
import cz.cvut.fit.vizeldim.dto.response.CategoryDTO;
import cz.cvut.fit.vizeldim.dto.response.PostDTO;
import cz.cvut.fit.vizeldim.dto.response.TagDTO;
import cz.cvut.fit.vizeldim.dto.response.page.CommentPageDTO;
import cz.cvut.fit.vizeldim.service.PostService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpServletRequest;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

@Controller
@RequestMapping("/posts")
public class PostController {
    private final PostClient postClient;
    private final TagClient tagClient;
    private final CommentClient commentClient;
    private final CategoryClient categoryClient;
    private final PostService postService;

    @Autowired
    public PostController(PostClient postClient,
                          TagClient tagClient,
                          CommentClient commentClient,
                          CategoryClient categoryClient, PostService postService)
    {
        this.postClient = postClient;
        this.tagClient = tagClient;
        this.commentClient = commentClient;
        this.categoryClient = categoryClient;
        this.postService = postService;
    }

    @RequestMapping("/{id}")
    public String post(Model model, @PathVariable Long id, @RequestParam(required = false) Integer commentPage){
        if (commentPage==null){commentPage=0;}else {--commentPage;}

        PostDTO postDTO = postClient.getItem(id);
        model.addAttribute("title", postDTO.title);
        model.addAttribute("post", postDTO);

        CommentPageDTO commentPageDTO = commentClient.getCommentsFromPost(id,commentPage,Constants.POSTS_PER_PAGE);
        model.addAttribute("comments",commentPageDTO.dtoList);

        if (commentPageDTO.totalPages == 0)
            commentPageDTO.totalPages = 1;

        model.addAttribute("uid", id);

        model.addAttribute("pages", commentPageDTO.totalPages);
        model.addAttribute("current_page", commentPage+1);

        CategoryDTO categoryDTO = categoryClient.getItem(postDTO.categoryId);
        model.addAttribute("category",categoryDTO);

        TagDTO[] tagDTOS = tagClient.getItems(postDTO.id);

        model.addAttribute("tags", tagDTOS);
        return "post";
    }

    @RequestMapping("/new")
    public String newPost(Model model){
        model.addAttribute("title","New Post");
        model.addAttribute("categories",categoryClient.getAll());
        model.addAttribute("tags", tagClient.getAll());
        return "newpost";
    }


    @RequestMapping("/add")
    public String addPost(String title,
                          String content,
                          long category,
                          @RequestParam Optional<Long[]> tags){
        PostCreateDTO post = new PostCreateDTO();
        post.title = title;
        post.content = content;
        post.categoryId = category;

        if (postService.validate(post))
        {
            Long postId = postClient.addPost(post);
            tags.ifPresent(longs -> postClient.addTags(postId, longs));
        }

        return "redirect:/admin";
    }

    @RequestMapping("/{id}/delete")
    public String deletePost(@PathVariable Long id, HttpServletRequest request){
        postClient.deletePost(id);

        return "redirect:/";
    }

    @RequestMapping("/{id}/edit")
    public String editPost(Model model, @PathVariable Long id){
        model.addAttribute("title","Edit Post");
        model.addAttribute("categories",categoryClient.getAll());
        model.addAttribute("post", postClient.getItem(id));

        TagDTO[] currentTags = tagClient.getItems(id);
        model.addAttribute("currentTags", currentTags);


        TagDTO[] allTags = tagClient.getAll();

        List<TagDTO> currentTagsList = Arrays.asList(currentTags);

        model.addAttribute("otherTags", Arrays
                .stream(allTags)
                .filter(e -> !currentTagsList.contains(e)).toArray());

        return "editpost";
    }

    @RequestMapping("/{id}/update")
    public String updatePost(@PathVariable Long id,
                             String title,
                             String content,
                             long category,
                             @RequestParam Optional<Long[]> addTags,
                             @RequestParam Optional<Long[]> removeTags){
        PostCreateDTO postCreateDTO = new PostCreateDTO();
        postCreateDTO.title = title;
        postCreateDTO.content = content;
        postCreateDTO.categoryId = category;

        if (postService.validate(postCreateDTO))
            postClient.update(id, postCreateDTO);

        removeTags.ifPresent(tags -> postClient.removeTags(id, tags));
        addTags.ifPresent(tags -> postClient.addTags(id, tags));

        return "redirect:/posts/"+id;
    }
}