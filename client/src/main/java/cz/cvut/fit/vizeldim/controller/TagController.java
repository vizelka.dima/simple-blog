package cz.cvut.fit.vizeldim.controller;

import cz.cvut.fit.vizeldim.client.PostClient;
import cz.cvut.fit.vizeldim.client.TagClient;
import cz.cvut.fit.vizeldim.dto.create.TagCreateDTO;
import cz.cvut.fit.vizeldim.dto.response.TagDTO;
import cz.cvut.fit.vizeldim.dto.response.page.PostPageDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
@RequestMapping("/tags")
public class TagController {
    private final TagClient tagClient;
    private final PostClient postClient;

    @Autowired
    public TagController(TagClient tagClient, PostClient postClient) {
        this.tagClient = tagClient;
        this.postClient = postClient;
    }

    @RequestMapping("/{id}")
    public String tag(Model model, @PathVariable int id, @RequestParam(required = false) Integer page){
        TagDTO tagDTO = tagClient.getItem(id);
        model.addAttribute("title", tagDTO.name);
        model.addAttribute("uid", tagDTO.id);

        if (page == null)
            page = 0;
        else
            --page;

        PostPageDTO postDTOList = postClient
                .getPageWithTag(tagDTO.id, page,Constants.POSTS_PER_PAGE, Constants.POST_INTRO_LENGTH);

        model.addAttribute("posts", postDTOList.dtoList);
        model.addAttribute("nextPageUrl", "tags");

        if(postDTOList.totalPages == 0)
            postDTOList.totalPages = 1;

        model.addAttribute("pages", postDTOList.totalPages);
        model.addAttribute("current_page", page+1);

        return "category";
    }

    @RequestMapping("/new")
    public String newTag(Model model){
        model.addAttribute("title","New Tag");

        return "newtag";
    }

    @RequestMapping("/add")
    public String addTag(@RequestParam String name){
        TagCreateDTO tagCreateDTO = new TagCreateDTO();
        tagCreateDTO.name = name;

        if (name.length() > 0)
            tagClient.addTag(tagCreateDTO);

        return "redirect:/admin";
    }
}