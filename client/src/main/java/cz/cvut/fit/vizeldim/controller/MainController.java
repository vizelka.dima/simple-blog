package cz.cvut.fit.vizeldim.controller;

import cz.cvut.fit.vizeldim.client.PostClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

@Controller
public class MainController {
    private final PostClient postClient;

    @Autowired
    public MainController(PostClient postClient) {
        this.postClient = postClient;
    }

    @RequestMapping("/")
    public String home(Model model){
        model.addAttribute("title","Welcome to REST-BLOG");
        model.addAttribute("posts", postClient
                .getPage(0,Constants.POSTS_PER_PAGE, Constants.POST_INTRO_LENGTH)
                .dtoList);

        return "home";
    }
}